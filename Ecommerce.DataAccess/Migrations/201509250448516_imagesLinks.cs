namespace Ecommerce.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class imagesLinks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ImageID = c.Int(nullable: false, identity: true),
                        ImageLink = c.String(),
                        Product_ProductID = c.Int(),
                    })
                .PrimaryKey(t => t.ImageID)
                .ForeignKey("dbo.Products", t => t.Product_ProductID)
                .Index(t => t.Product_ProductID);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        VideoID = c.Int(nullable: false, identity: true),
                        VideoLink = c.String(),
                        Product_ProductID = c.Int(),
                    })
                .PrimaryKey(t => t.VideoID)
                .ForeignKey("dbo.Products", t => t.Product_ProductID)
                .Index(t => t.Product_ProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Videos", "Product_ProductID", "dbo.Products");
            DropForeignKey("dbo.Images", "Product_ProductID", "dbo.Products");
            DropIndex("dbo.Videos", new[] { "Product_ProductID" });
            DropIndex("dbo.Images", new[] { "Product_ProductID" });
            DropTable("dbo.Videos");
            DropTable("dbo.Images");
        }
    }
}
